import random
#I write the train.txt and validate.txt files and put them in gunData
def writejpgs(howMany, jpgPath):
    jpgNames = ""
    x = 1
    while x <= howMany:
        jpgNames += jpgPath + "armas (" + str(x) + ").jpg\n"
        x += 1
    return jpgNames

def writeFiles(file, text):
    try:
        f = open(file, "w")
        f.write(text)
        f.close()
    except:
        print("Error opening " + str(file))

def arrToString(arr):
    text = ""
    for line in arr:
        text += line + "\n"
    return text

def randomizeData(text):
    arr = text.splitlines()
    arrTrain = []
    arrValidate = []
    trainPortion = int(round(len(arr) * 0.9))
    validatePortion = len(arr) - trainPortion

    print("Train Portion: " + str(trainPortion))
    print("Validate Portion: " + str(validatePortion))

    x = 0
    while(x < trainPortion):
        arrTrain.append(arr.pop(random.randint(0, len(arr) - 1)))
        x += 1
    x = 0
    while(x < validatePortion):
        arrValidate.append(arr.pop(random.randint(0, len(arr) - 1)))
        x += 1

    return [arrTrain, arrValidate]

def makeTxtFiles(howMany, jpgPath, fwriteTrain, fwriteValidate):
    print("Attempting to write data...")
    data = writejpgs(howMany, jpgPath)
    dataSets = randomizeData(data)
    writeFiles(fwriteTrain, arrToString(dataSets[0]))
    writeFiles(fwriteValidate, arrToString(dataSets[1]))
    print("Suceeded!")

#   Call Function
makeTxtFiles(3000, "yolov4-training/gunData/", "gunData/train.txt", "gunData/validate.txt")
